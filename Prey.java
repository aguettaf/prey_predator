package prey_predator;

import java.util.Random;

/**
 * @author Abdelkader_Guettaf
 */

public class Prey extends Animal {
	
	
	/**Les attributs
	 * 
	 * averageEscapeRate : moyenne d'�chappement, double, avec une valeur par default pour proie, une visibilit� priv�e
	 * stdDevEscapeRate :�cart type d'�chappement, double, avec une valeur par default pour proie, une visibilit� priv�e
	 * escapeRate : double, une visibilit� priv�e
	 */

	private double averageEscapeRate = 0.7 ;
	private double stdDevEscapeRate = 0.1 ;
	private double escapeRate;
	
	
	
	/** getAverageEscapeRate()
	 * 
	 * @return double : averageEscapeRate: moyenne d'�chappement
	 */
	
	double getAverageEscapeRate(){
		return averageEscapeRate ;
	}
	
	/**getStdDevEscapeRate()
	 * 
	 * @return double : stdDevEscapeRate: l'�cart type d'�chappement
	 */
	
	double getStdDevEscapeRate(){
		return stdDevEscapeRate ;
	}
	
	/**getEscapeRate()
	 * 
	 * @return double : escapeRate: Taux d'�chappement
	 */
	
	double getEscapeRate(){
		return escapeRate ;
	}
	
	/**setAverageEscapeRate(double averageEscapeRate)
	 * 
	 * @param averageEscapeRate: la nouvelle valeur de la moyenne d'echappement
	 */
	
	void setAverageEscapeRate(double averageEscapeRate){
		this.averageEscapeRate = averageEscapeRate ;
	}
	
	/**setStdDevEscapeRate(double stdDevEscapeRate)
	 * 
	 * @param stdDevEscapeRate: double : la nouvelle valeur de l'ecart type d'echappement
	 */
	
	void setStdDevEscapeRate(double stdDevEscapeRate){
		this.stdDevEscapeRate = stdDevEscapeRate ;
	}
	
	/**setEscapeRate(double escapeRate)
	 * 
	 * @param escapeRate : double : nouveau taux d'�chappement
	 */
	
	void setEscapeRate(double escapeRate){
		this.escapeRate = escapeRate ;
	}
	
	
	
	/**Constructeur : Prey
	 * 
	 * Construction des objet dans les attributs en commun en appelant le constructeur : Animal(double avRR, double sdRR, double avLE, double sdLE) de la supper-Classe, avec des valeurs fixe 
	 * Calcule et construction Taux d'echappement specifique pour : Proie
	 */
	
	Prey(){
		super(0.9, 0.1, 10.0, 1.0);
		
		Random r = new Random();
		setEscapeRate(r.nextGaussian() * getStdDevEscapeRate () + getAverageEscapeRate()) ;
		if (getEscapeRate () < 0) {
			setEscapeRate(0);
		}
		if (getEscapeRate () > 1) {
			setEscapeRate(1);
		}
	}
	
	
	/**Prey(double avRR, double sdRR, double avLE, double sdLE, double avER, double sdER)
	 * 
	 * construction d'un objet en appelant le consturcteur Animal(double avRR, double sdRR, double avLE, double sdLE) de la classe principale
	 * et on calcule le taux d'�chappement (attribut sp�cifique pour cette classe)
	 * 
	 * @param avRR: double : la nouvelle valeur de la moyenne de la  reproduction
	 * @param sdRR: double :la nouvelle valeur de l'ecart type de la reproduction
	 * @param avLE: double :la nouvelle valeur de la moyenne de  l'esperence de la vie
	 * @param sdLE: double :la nouvelle valeur de l'ecart type de l'esperence de la vie
	 * @param avER double : la nouvelle valeur de la moyenne de taux d'�chappement
	 * @param sdER double : la nouvelle valeur de l'ecart type de taux d'echappement
	 */
	
	
	Prey(double avRR, double sdRR, double avLE, double sdLE, double avER, double sdER){
		super(avRR, sdRR, avLE, sdLE); 
		
		Random r = new Random();
		setEscapeRate (r.nextGaussian() * getStdDevEscapeRate () + getAverageEscapeRate ()) ;
		if (getEscapeRate () < 0) {
			setEscapeRate (0);
		}
		if (getEscapeRate () > 1) {
			setEscapeRate (1);
		}
	}
	
	
	
	/**String toString()
	 * 
	 *  Affichage de l'objet proie cr��
	 */
	
	
	public String toString() {
		String s = super.toString();
		s += "Taux d'�chappement : " + escapeRate;
		return s;
	}
	
	
	
	/** isAbleToEscape()
	 * 
	 * @return boolean: true: proie capable d'echapper (taux d'echappement > nombre aleatoire), false: proie incapable d'echapper (taux d'echappement < nombre aleatoire)
	 */
	
	
	boolean isAbleToEscape() {
		Random r = new Random() ;
		return (r.nextDouble() < getEscapeRate());
	}
	
	
		
	public static void main(String[] args) {
		
		Prey piaf = new Prey() ;
		System.out.println(piaf);
	}
	
	
}
