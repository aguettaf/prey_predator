package prey_predator;

import java.util.Random;

/**
 * @author Abdelkader_Guettaf
 */

public class Animal {
	
	/**
	 * Ce fichier de Java cr�e une exemple des proies et pr�dateurs, en utilisant 
	 * l'heritage � partir d'une classe principale Animal
	 */
	
	
	/**les attributs - priv�e
	 * 
	 * averageReproductionRate : moyen de reproduction      (double avec une visibilit� priv�e)
	 * stdDevReproductionRate  : ecart type de reproduction (double avec une visibilit� priv�e)
	 * 
	 * averageLifeExpectancy   : moyen de l'esperence de la vie      (double avec une visibilit� priv�e)
	 * stdDevLifeExpectancy    : ecart type de l'esperence de la vie (double avec une visibilit� priv�e)
	 * 
	 * Age : l'age de l'animal (integer avec une visibilit� priv�e)
	 * lifeExpectancy          : l'esp�rence de la vie   (double avec une visibilit� priv�e)
	 * reproductionRate        : le taux de reproduction (double avec une visibilit� priv�e)
	 * 
	 * **/
	
	
	private double averageReproductionRate = 0.3 ;
	private double stdDevReproductionRate = 0.1 ;
	
	private double averageLifeExpectancy = 10 ; 
	private double stdDevLifeExpectancy = 4 ; 
	
	private int age ;
	private double reproductionRate ;
	private double lifeExpectancy ;
	
	
	
	
	  
	/** getReproductionRate() 
	 * 
	 * @return reproductionRate: le taux de la reproduction
	 */
	
	double getReproductionRate() {
		return reproductionRate;
	}
	
	/** getLifeExpectancy() 
	 * 
	 * @return reproductionRate : la valeur de l'esperence de la vie 
	 */
	
	double getLifeExpectancy() {
		return lifeExpectancy ;
	}
	
	/** getAge() 
	 * 
	 * @return age : l'age de l'animal
	 */
	
	int getAge() {
		return age ;
	}
	
	
	/** getAverageReproductionRate() 
	 * 
	 * @return averageReproductionRate : Moyenne de reproduction
	 */
	
	double getAverageReproductionRate() {
		return averageReproductionRate ;
	}
	
	
	/** getStdDevReproductionRate() 
	 * 
	 * @return stdDevReproductionRate : l'ecart type de  la reproduction
	 */
	
	double getStdDevReproductionRate() {
		return stdDevReproductionRate ;
	}
	
	
	/** getAverageLifeExpectancy() 
	 * 
	 * @return averageLifeExpectancy : la moyenne de l'esperence de la vie
	 */
	
	double getAverageLifeExpectancy() {
		return averageLifeExpectancy ;
	}
	
	
	/** getStdDevLifeExpectancy() 
	 * 
	 * @return stdDevLifeExpectancy : l'ecart type de l'esperence de la vie
	 */
	
	double getStdDevLifeExpectancy() {
		return stdDevLifeExpectancy ;
	}
	
	
	/**Setters**/
	
	
	/**setReproductionRate (double reproductionRate) 
	 * 
	 * @param reproductionRate : nouvelle valeur du taux de la reproduction
	 */
	
	
	void setReproductionRate(double reproductionRate) {
		this.reproductionRate = reproductionRate;
	}
	
	
	/**setLifeExpectancy (double lifeExpectancy)
	 * 
	 * @param lifeExpectancy : nouvelle valeur de l'esperence de la vie
	 */
	
	
	protected void setLifeExpectancy(double lifeExpectancy) {
		this.lifeExpectancy = lifeExpectancy;
	}
	
	
	/**setAge(int age)
	 * 
	 * @param Age : nouvelle valeur de l'age de l'animal
	 */
	
	void setAge(int age) {
		this.age = age ;
	}
	
	
	/**setAverageReproductionRate (double averageReproductionRate)
	 * 
	 * @param averageReproductionRate : nouvelle valeur de la moyenne de la reproduction
	 */
	
	void setAverageReproductionRate(double averageReproductionRate) {
		this.averageReproductionRate = averageReproductionRate ;
	}
	
	
	/**setStdDevReproductionRate( double stdDevReproductionRate)
	 * 
	 * @param stdDevReproductionRate : nouvelle valeur de l'ecart type de la reproduction
	 */
	
	void setStdDevReproductionRate( double stdDevReproductionRate) {
		this.stdDevReproductionRate = stdDevReproductionRate  ;
	}
	
	
	/**setAverageLifeExpectancy(double averageLifeExpectancy)
	 * 
	 * @param averageLifeExpectancy : nouvelle valeur de la moyenne de l'esperence de la vie
	 */
	
	void setAverageLifeExpectancy(double averageLifeExpectancy) {
		this.averageLifeExpectancy = averageLifeExpectancy ;
	}
	
	
	/**setStdDevLifeExpectancy(double stdDevLifeExpectancy)
	 * 
	 * @param stdDevLifeExpectancy : nouvelle valeur de l'ecart type de l'esperence de la vie
	 */
	
	void setStdDevLifeExpectancy(double stdDevLifeExpectancy) {
		this.stdDevLifeExpectancy = stdDevLifeExpectancy;
	}
	 
	
	
	
	
	/**Animal 
	 * 
	 * Polymorphisme:
	 * @see #Animal()
	 * 
	 * pour calculer reproductionRate et lifeExpectancy (n gausien (-1,1) alea*ecartype+moyen) ainsi que l'age, et remplir leurs attributs 
	 * 
	 * @param avRR: la nouvelle valeur de la moyenne de la  reproduction
	 * @param sdRR: la nouvelle valeur de l'ecart type de la reproduction
	 * @param avLE: la nouvelle valeur de la moyenne de  l'esperence de la vie
	 * @param sdLE: la nouvelle valeur de l'ecart type de l'esperence de la vie
	 */
	
	
	Animal(double avRR, double sdRR, double avLE, double sdLE){
		
		setAverageReproductionRate(avRR) ;
		setStdDevReproductionRate(sdRR) ;
		setAverageLifeExpectancy(avLE) ;
		setStdDevLifeExpectancy(sdLE) ;
		
		
		Random r = new Random() ;
		
		setReproductionRate (r.nextGaussian() * getStdDevReproductionRate () + getAverageReproductionRate ()) ;
		if (getReproductionRate() < 0) {
			setReproductionRate (0);
		}
		if (getReproductionRate() > 1) {
			setReproductionRate (1);
		}
		
		
		setLifeExpectancy ( r.nextGaussian() * getStdDevLifeExpectancy () + getAverageLifeExpectancy ());
		setAge ((int)(r.nextDouble() * getLifeExpectancy()));
		
	
		if (getLifeExpectancy () < 0) {
			setLifeExpectancy (0) ;
		}
		if (getLifeExpectancy () > 1) {
			setLifeExpectancy (1);
		}
	
	}
	
	
	/**Animal
	 * 
	 * Polymorphisme:
	 * @see Animal #Animal(double, double, double, double)
	 *  
	 *pour calculer reproductionRate et lifeExpectancy (nombre aleatoire gausien (-1,1)*ecartype+moyen) ainsi que l'age, avec les valeurs par default, et remplir leurs attributs 

	 **/
	
	
	Animal(){
		
		Random r = new Random() ;
		
		setReproductionRate (r.nextGaussian() * getStdDevReproductionRate () + getAverageReproductionRate ()) ;
		
		if (getReproductionRate () < 0) {
			setReproductionRate(0);
		}
		if (getReproductionRate () > 1) {
			setReproductionRate (1);
		}
		
		
		setLifeExpectancy (r.nextGaussian() * getStdDevLifeExpectancy () + getAverageLifeExpectancy ());
		
		setAge((int)(r.nextDouble() * getLifeExpectancy()));
		
		if (getLifeExpectancy () < 0) {
			setLifeExpectancy (0);
		}
		if (getLifeExpectancy () > 1) {
			setLifeExpectancy (1);
		}
	}
	
	
	
	/**isAbleToReproduce
	 * 
	 * @return: boolean : true taux de roproduction <nombre aleatoire ; faux taux de reproduction > nombre aleatoire
	 */
	
	public boolean isAbleToReproduce() {
		return Math.random() < getReproductionRate() ;
	}
	
	
	/**isAlive
	 * 
	 * @return boolean: true : si l'age <l'esperence de la vie , false: age> l'esperence de la vie
	 */
	
	public boolean isAlive() {
		return (getAge() < getLifeExpectancy()) ;
	}
	
	
	/**incrementAge
	 * 
	 * Incr�menter l'age par un an
	 */
	
	public void incrementAge() {
		age += 1 ;
	}
	
	
	
	/**toString
	 * 
	 * @return s: Affichage des objets instanci�s
	 * 
	 */
	
	public String toString() {
		String s = "**** Animal *****\n";
		s += "Age :"+ getAge() + " pour une esp�rance de vie de :" + getLifeExpectancy() + "\n";
		s += "Taux de reproduction de :" + getReproductionRate() + "\n";
		return s ;
	}
	
	
	
	
	public static void main(String[] args) {
		
		Animal trotro = new Animal();
		System.out.println(trotro);
		
		Animal picsou = new Animal(30,5,0.2,0.1);
		System.out.println(picsou);
	}




}
