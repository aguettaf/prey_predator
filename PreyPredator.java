package prey_predator;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * @author Abdelkader_Guettaf
 */

public class PreyPredator {
	

	
	
	/**les attributs
	 * 
	 * proie : Linkedlist : collection dynamique, ordonn�s par num�rotation, 
	 * accepte les dedoublement (on a utilis� liste pour benificier des m�thodes de cette classe)
	 * 
	 * predateurs : HashSet : collection dynamique, non ordonn�s, n'accepte pas le d�doublement
	 * (on a utilis� Set pour benificier des m�thodes de cette classe)
	 * 
	 * proiesMax :integer: nombre maximum de la liste des proies
	 * 
	 * year: integer: initionation de nombre des ann�es
	 */
	
	
	 List<Prey> proies = new LinkedList<Prey>(); 
	 Set<Predator> predateurs = new HashSet<Predator>(); 
	
	 int proiesMax = 3000 ;
	 int year = 0 ;
	
	 
	
	 
	/**PreyPredator(int npreys, int npred)
	 * 
	 * Construction des instances dans les deux collections
	 * 
	 * @param npreys : nombre d'�lement (objet) dans  la collection des proies
	 * @param npred : nombre d'�l�ment (objet) dans la collection des predateurs
	 */
	
	
	PreyPredator(int npreys, int npred){
		for (int i=0 ; i< npreys ; i++){
			Prey p = new Prey();
			proies.add(p);
		}
		for (int i=0 ; i< npred ; i++){
			Predator p = new Predator();
			predateurs.add(p);
		}
	}
	
	
	
	
	/**PreyPredator(int npreys, double avr, double sdr, double avl, double sdl, double ave, double sde, int npred, double avrP, double sdrP, double avlP, double sdlP, double avaP, double sdaP)
	 * 
	 * cr�ation des objets dans les instances avec des valeurs introduites pour les proies et pour les pr�dateurs
	 * 
	 * @param npreys: nombre des instances dans la collection des proies
	 * @param avr : la moyenne de la  reproduction de proie
	 * @param sdr : l'�cart type de la reproduction de proie
	 * @param avl : la moyenne de  l'esperence de la vie de proie
	 * @param sdl : l'ecart type de l'esperence de la vie de proie
	 * @param ave : la moyenne de taux d'�chappement de proie
	 * @param sde : l'ecart type de taux d'echappement de proie
	 * 
	 * @param npred: nombre des instances dans la collection des pr�dateurs
	 * @param avrP : la moyenne de la  reproduction de pr�dateur
	 * @param sdrP : l'�cart type de la reproduction de pr�dateur
	 * @param avlP : la moyenne de  l'esperence de la vie de pr�dateur
	 * @param sdlP : l'ecart type de l'esperence de la vie de pr�dateur
	 * @param avaP : la moyenne de taux d'attack de pr�dateur
	 * @param sdaP : l'ecart type de taux d'attack de pr�dateur
	 */
	
	
	PreyPredator(int npreys, double avr, double sdr, double avl, double sdl, double ave, double sde, int npred, double avrP, double sdrP, double avlP, double sdlP, double avaP, double sdaP){
		for (int i=0 ; i< npreys ; i++){
			Prey p = new Prey(avr,sdr,avl,sdl,ave,sde);
			proies.add(p);
		}
		for (int i=0 ; i< npred ; i++){
			Predator p = new Predator(avrP,sdrP,avlP,sdlP,avaP,sdaP);
			predateurs.add(p);
		}
	}
	
	
	
	
	/**String toString()
	 * 
	 * @return s: Affichage de la taille du Set :pr�dateurs et la taille de la liste proies dans un temps pr�cis
	 */
	
	
	public String toString(){
		String s = "Etat du systeme : " ;
		s += "Au temps t= "+this.year+" proies= "+proies.size()+" predateurs= "+predateurs.size();
		return s;
	}
	
	
	
	
	/**oneYear ()
	 * 
	 * 1-Calcule le nombre des b�b�s pour les proies et les pr�dateurs cr�es dans les collections
	 * 
	 * 
	 * 2-Remplir le vide entre la taille actuelle de la liste des proies et son limite maximale par 
	 * les nouvels instances des b�b�s (Math.min va choisir la valeur minimal entre les deux, pour 
	 * quelle soit capable de le contenir), o� chaque 2 proies b�b�s donne un membre dans la liste des proies 
	 * 
	 * Remplir aussi le Set des pr�dateurs par les b�b�s cre�s (2 b�b�s =1), sans limite de Set 
	 * 
	 * 
	 * 3-Pour chaque pr�dateur s'il peut attaquer ,et s'il y a encore des proies � attaquer,
	 * et si le proie ne peut pas �chapper: le proie va etre retir� de la liste, 
	 * sinon il y'aura pas un changement dans liste des proies 
	 *
	 * 4-On cr�e une liste pour la prochaine ann�e pour les proies, et un Set pour la prochaine ann�e pour les pr�dateurs,
	 * On incr�mente la vie et si le predateur/proie reste vivant on l'ajoute � son collection
	 * et on remplace nos collections proies/predateurs par les nouvelles nextYear collections
	 *
	 */
	
	
	
	public void oneYear(){
		
		double babyPreys = 0 ;
		for (Prey p : proies) 
			if (p.isAbleToReproduce())
				babyPreys += 0.5 ;
		for (int i =0; i< Math.min(babyPreys,proiesMax-proies.size()); i++){
			Prey b = new Prey(); 
			proies.add(b);
		}
	
		double babyPreds = 0 ;
		for (Predator p : predateurs)
			if (p.isAbleToReproduce())
				babyPreds += 0.5 ;
		for (int i =0; i< babyPreds; i++){
			Predator b = new Predator(); 
			predateurs.add(b);
		}
		
		
		Random r = new Random() ;
		for (Predator p : predateurs){ 
			if (p.canAttack()){ 
				if (proies.size() > 0){  
					Prey repas = proies.get(r.nextInt(proies.size()));
					if (repas.isAbleToEscape() == false)
						proies.remove(repas);
					else {
						p.starvation();
						}
				}
				else{
					p.starvation();
				}
			}
		}
	
		
		List<Prey> nextYearPreys = new LinkedList<Prey>();
		for (Prey p : proies){
			p.incrementAge();
			if (p.isAlive())
				nextYearPreys.add(p);
		}
	
		Set<Predator> nextYearPreds = new HashSet<Predator>();
		for (Predator p : predateurs){
			p.incrementAge();
			if (p.isAlive())
				nextYearPreds.add(p);
		}
		proies = nextYearPreys;
		predateurs = nextYearPreds;
		year += 1 ;
	}
	
	
	
	public static void main(String[] args) {

		PreyPredator pp = new PreyPredator(20,400);
		
		for (int y=0; y<1000; y++){
			System.out.println(pp);
			pp.oneYear();
		}
	}


}
