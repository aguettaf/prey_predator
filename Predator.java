package prey_predator;

import java.util.Random;

/**
 * @author Abdelkader_Guettaf
 */

public class Predator extends Animal {
	
	
	/**les attributs
	 * 
	 * double averageAttackRate : double priv�e : moyenne d'attaque
	 * stdDevAttackRate : double priv�e : �cart type d'attaque
	 * attackRate: double priv�e : taux d'attaque 
	 */
	
	
	private double averageAttackRate = 0.9 ;
	private double stdDevAttackRate = 0.1 ;
	private double attackRate;
	
	
	
	
	/**getAverageAttackRate()
	 * 
	 * @return double : moyenne d'attaque
	 */
	
	double getAverageAttackRate(){
		return averageAttackRate ;
	}
	
	
	/**getStdDevAttackRate()
	 * 
	 * @return: double : l'�cart type d'attaque
	 */
	
	double getStdDevAttackRate(){
		return stdDevAttackRate ;
	}
	
	/**getAttackRate()
	 * 
	 * @return : double ; taux d'attaque
	 */
	
	double getAttackRate(){
		return attackRate ;
	}
	
	
	
	
	
	/**setAverageAttackRate (double averageAttackRate)
	 * 
	 * @param double averageAttackRate: nouvelle moyenne d'attaque
	 */
	
	void setAverageAttackRate (double averageAttackRate){
		this.averageAttackRate = averageAttackRate ;
	}
	
	/**setStdDevAttackRate (double stdDevAttackRate)
	 * 
	 * @param double stdDevAttackRate : nouvel ecart type d'attaque
	 */
	
	void setStdDevAttackRate (double stdDevAttackRate){
		this.stdDevAttackRate = stdDevAttackRate ;
	}
		
	/**setAttackRate (double attackRate)
	 * 
	 * @param attackRate: double : Taux d'attaque
	 */
	
	void setAttackRate (double attackRate){
		this.attackRate = attackRate ;
	}
	
	
	
	
	/**Constructeur:  Predator()
	 * 
	 * construction d'un objet dans la classe Predator : en appelant le constructeur : Animal(double avRR, double sdRR, double avLE, double sdLE), de la classe principale en utilisant des valeurs fixes
	 * apr�s on calcule et on ajoute l'attribut specifique � Predator (taux d'attaque)
	 *
	 */
	
	Predator(){
		super(0.4, 0.1, 12.0, 3.0) ; 
		
		Random r = new Random();
		setAttackRate (r.nextGaussian() * getStdDevAttackRate() + getAverageAttackRate()) ;
		if (getAttackRate () < 0) {
			setAttackRate (0) ;
		}
		if (getAttackRate () > 1) {
			setAttackRate (1) ;
		}
	}
	
	
	
	
	/**Predator(double avRR, double sdRR, double avLE, double sdLE, double avAR, double sdAR)
	 * 
	 * construction d'un objet dans la classe Predator : en appelant le constructeur : Animal(double avRR, double sdRR, double avLE, double sdLE), de la classe principale en utilisant des valeurs introduites par l'utilisateur
	 * apr�s on calcule et on ajoute l'attribut specifique � Predator (taux d'attaque)
	 * 
	 * @param avRR: double : la nouvelle valeur de la moyenne de la  reproduction
	 * @param sdRR: double :la nouvelle valeur de l'ecart type de la reproduction
	 * @param avLE: double :la nouvelle valeur de la moyenne de  l'esperence de la vie
	 * @param sdLE: double :la nouvelle valeur de l'ecart type de l'esperence de la vie
	 * @param avAR: double :la nouvelle valeur de la moyenne de taux d'attack
	 * @param sdAR: double :la nouvelle valeur de l'ecart type de taux d'attaque
	 */
	
	Predator(double avRR, double sdRR, double avLE, double sdLE, double avAR, double sdAR){
		super(avRR, sdRR, avLE, sdLE) ; 
		
		setAverageAttackRate (avAR) ;
		setStdDevAttackRate (sdAR) ;
		Random r = new Random() ;
		setAttackRate (r.nextGaussian() * getStdDevAttackRate () + getAverageAttackRate ()) ;
		if (getAttackRate () < 0) {
			setAttackRate (0);
		}
		if (getAttackRate () > 1) {
			setAttackRate  (1);
		}
	}
	
	
	
	
	/**String toString()
	 * 
	 * @return s : Affichage de l'instance cr�e dans la classe Predator
	 * 
	 */
	
	public String toString() {
		String s = super.toString();
		s += "Taux d'attaque : " + attackRate;
		return s;
	}
	
	
	
	
	/**canAttack()
	 * 
	 * @return boolean: true : si taux d'attaque > nombre aleatoire, false :si taux d'attaque < nombre aleatoire,
	 */
	
	
	boolean canAttack() {
		Random r = new Random() ;
		return (r.nextDouble() < getAttackRate());
	}
	
	
	
	
	/**starvation()
	 * 
	 * Changer la valeur de l'esperance de la vie dans la classe principale : Animal
	 * (on a changer la visibilit� de setLifeExpectancy Protected pour avoir la possibilit� de la changer ici)
	 */
	
	
	void starvation() {
		setLifeExpectancy(getLifeExpectancy() - 2) ; 
	}
	
	
	
	
	public static void main(String[] args) {
		Prey cocotte = new Prey() ;
		System.out.println(cocotte) ;
		
		Predator leLoup = new Predator() ;
		System.out.println(leLoup) ;
	}
	
	
	

}
